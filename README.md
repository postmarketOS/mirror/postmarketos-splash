# postmarketos-splash

postmarketos-splash was our previous splash screen generator. It is no longer
compatible with Pillow as of version 10, and has been replaced by
[pbsplash](https://git.sr.ht/~calebccff/pbsplash) in postmarketOS.
